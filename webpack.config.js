const path = require("path");

module.exports = {
    entry: "./lib/app.js",
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'application.js'
    }
};