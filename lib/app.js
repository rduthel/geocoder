import L from "leaflet";

const flexElt = document.getElementsByClassName("flexbox")[0];
const formElt = document.forms.geocoder;

const latElt = document.getElementById("lat");
const lonElt = document.getElementById("lon");

let map = null;

formElt.addEventListener("submit", (event) => {
  event.preventDefault();

  if (map !== null) {
    map.remove();
  }

  fetch(`https://nominatim.openstreetmap.org/search?q=${formElt.elements.search.value}&format=json&polygon=1&addressdetails=1`).then(response => response.json()).then((data) => {
    data = data[0];

    const latPlace = data.lat;
    const lonPlace = data.lon;
    const boundingBox = data.boundingbox;

    map = L.map("map").fitBounds([
      [boundingBox[0], boundingBox[2]],
      [boundingBox[1], boundingBox[3]]
    ]);

    const marker = L.marker([
      latPlace, lonPlace
    ], { draggable: true });

    marker.on('dragend', (e) => {
      console.log(marker.getLatLng());
    });

    latElt.innerHTML = latPlace;
    lonElt.innerHTML = lonPlace;

    L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png", { attribution: "&copy; <a href='http://osm.org/copyright'>OpenStreetMap</a> contributors" }).addTo(map);

    marker.addTo(map)
      .bindPopup(data.display_name)
      .openPopup();
  });
});
